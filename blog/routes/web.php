<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// First route that user visits on consumer app
/*Route::get('/', function () {
    // Build the query parameter string to pass auth information to our request
    $query = http_build_query([
        'client_id' => 3,
        'redirect_uri' => 'http://consumer.dev/callback',
        'response_type' => 'code',
        'scope' => 'conference'
    ]);

    // Redirect the user to the OAuth authorization page
    return redirect('http://localhost/oauth/authorize?' . $query);
});*/



Route::get('/api/test', 'OrdersController@StoreOrder');


Route::get('/authenticate', function () {   
    
    $guzzle = new GuzzleHttp\Client;

    $response = '';

    try {
        $response = $guzzle->post('http://127.0.0.1/oauth/token', [
            'form_params' => [
                'grant_type' => 'client_credentials',
                'client_id' => '2',
                'client_secret' => 'LSL3fXLKPTVAIbb3ujGqvKSAUgZjRluliDqM6gmi',
                'scope' => '*',
            ],
            'headers' => [
                'Accept'     => 'application/json',
                'Host'       => 'server.local'
            ]
        ]);
    } catch (Exception $e) {
        echo 'Caught exception: ',  $e->getMessage(), "\n";
        return;
    }

    return $response->getBody();
});

Auth::routes();


Route::get('/home', 'HomeController@index')->name('home');

