<?php

namespace App\Http\Controllers;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\Order;

class OrdersController extends Controller
{
    public function StoreOrder()
    {

        $api_token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6Ijk1YWY3MmE4ZGYxN2FiOTMzNWY4MjM5NDNlZmI0ZGU3ZjI4OGJkOWNkY2Y0YWMyYWYxYWM3ODgwN2ZmZjYzZThmNDA4MDc3MTE5YjJlY2RiIn0.eyJhdWQiOiIyIiwianRpIjoiOTVhZjcyYThkZjE3YWI5MzM1ZjgyMzk0M2VmYjRkZTdmMjg4YmQ5Y2RjZjRhYzJhZjFhYzc4ODA3ZmZmNjNlOGY0MDgwNzcxMTliMmVjZGIiLCJpYXQiOjE1Mzk1OTM1OTIsIm5iZiI6MTUzOTU5MzU5MiwiZXhwIjoxNTcxMTI5NTkyLCJzdWIiOiIiLCJzY29wZXMiOltdfQ.hnNEv3malijNmqOyS0fAWqCvtkNVkX-gqWMdNZm3foswUaszN_QoAVACCZlirB0mXDamWBfePjJH2-34m6P6wWBVk95qYndSgwgSab4kQqtnv02q9d2Lt3jndHjFF6DpXqVHSnVjrC6RmKI55umCWmt1Ig-q_fCAvhQZv8VabrigsqepVh_6uvVDMDDZo8k6UFJnzykOkMPlaWhWC8aC3091auO7rlnT4NvdxDn_P2bWkmtNIJTOvJ7TfY9f0gJtjcy_xGTjrYTHfdEYl0Mw0Mzmvpi939W0SZ5In7xChRUFL19s4ydpLshQMvT5OhMdVp5nsogO3DJiYCCIk8i-3fDf1TTFsltjBTNJILhNCMlLtQ8oKXAA1tS3VyV-XpmuKNOqekOe5QHDgJqNHWhmI_-J9BiT2pkHqTGi83ScYADbbtW-qdYBPnxan0g3q5gVAYs88th4lnhfoxFX7lAtMBtU6s9CEYzsruydancGHJRXTcjZ93LqvXDVc5Y_h6AeL-8bvAKD4V1e9la8MhsLcfx9tl1QL1xBcj0g_kGu9D4a8QO7PsngdzbePfRGO5lI59AGMmKfkTCvkbx2HMc7uYhnwQLAu80QUC9DnWPE74_R4syRgDOHhPExXAw_urtN_G544lCeBzefuwOrRWDCVztC_tkFPXok1OLsIIfZ13E";

        $guzzle = new Client;

        $response = '';
    
        try {

            $response = $guzzle->get('http://127.0.0.1/api/exserv/orders', [
                'headers' => [
                    'Accept'     => 'application/json',
                    'Host'       => 'server.local',
                    'Authorization' => 'Bearer '.$api_token.'', 
                ]
            ]);    
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
            return;
        }


        //Deserialize json to order object

        $order = new Order();

        dd($response->getBody());
        
        $OrderArray = $order->fromJson($response->getBody());

        return var_dump($OrderArray);
        
        //return json_decode((string) $response->getBody(), true)['access_token'];    
    
    
        //return view('welcome');
        }

}

